import React, { Component } from 'react';
import {Image, StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList, Dimensions} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class dongeng1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nurul : [
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Disebuah hutan belantara yang luas, tinggal beraneka ragam satwa. Salah satunya seekor kancil. Kancil yang satu ini dikenal memiliki kecerdikan yang luar biasa. Tak hanya cerdik, kancil pun dikenal sebagai satwa yang ramah akan sesama.Seperti suatu pagi, ia melihat seekor induk bebek yang tengah berenang bersama anaknya. Kancil pun yang sedang berjalan menelusuri hutan menyapa bebek tersebut,“Hai bebek! Asik sekali kamu berenang.”Begitu pun kepada satwa-satwa lain yang sepanjang jalan tak henti menyapa dan ia sapa. Itulah kancil dengan keramahan yang selalu disegani banyak satwa di hutan itu.</Text>,no:2},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Tak hanya ramah, kancil yang terkenal akan kecerdikannya ini juga sering membantu satwa-satwa hutan dalam memecahkan masalah. Banyak satwa yang datang ke kancil jika mereka memiliki masalah, kancil pun dengan senang hati membantu dan memecahkan masalah kawan-kawannya. Seperti suatu ketika, saat tengah berjalan menelusuri hutan ia menemukan tiga ekor anak ayam yang terperangkap di dalam lubang yang cukup dalam bagi mereka.Kancil pun segera menghampiri anak ayam itu dan turun ke lubang tersebut untuk membantu mereka yang terperangkap. Setelah masuk ke dalam, kancil membungkukkan badannya dan meminta anak ayam itu untuk menaiki tubuhnya.“Ayo! Kalian bisa naik ke tubuhku, jadi kalian bisa keluar dari ini dan bertemu induk kalian.” Ucap kancil ramah. Setelah semua anak ayam berhasil naik ke tubuhnya, kancil pun melompat keluar lubang dan berjalan menemui induk ayam yang tengah kebingungan mencari anak-anaknya. Ia pun melepaskan ketiga anak ayam tadi kepada induknya.</Text>,no:3},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Sang induk pun sangat senang dan berterima kasih kepada kancil yang sudah membantu anak mereka. Tak hanya induk ayam, ketiga ayam tadi juga bersorak kompak mengucapkan  terima kasih kepada kancil, “Terima kasih tuan kancil!” ucap ketiga anak ayam itu girang. Setelah berjalan-jalan, kancil pun mulai merasa lapar. Ia menepi untuk memakan rumput yang ada di sekitarnya. Setelah makan rumput, kancil berkata, “Rmput saja ternyata tidak membuat ku kenyang.”Kancil kemudian terus berjalan di tengah teriknya matahari hingga dirinya tiba di sebuah sungai  yang ada di dalam hutan. Ia mendekati tepi sungai untuk sekedar menghilangkan dahaga setelah berjalan-jalan dan makan tadi.Setelah puas minum, kancil yang tengah memandangi sungai langsung berbinar ketika melihat sesuatu yang menarik ada di sebrang sungai. Hal yang membuatnya tertarik adalah pohon buah-buahan yang dapat menghilangkan rasa laparnya tadi.Namun derasnya air sungai tidak memungkinkan kancil untuk menyebrangi sungai tersebut. Ia pun mencari cara agar bisa menyebrangi tanpa bahaya. Sampai suatu ide ia dapatkan yaitu dengan mengelabui buaya-buaya yang ada di sungai itu.</Text>,no:4},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Seekor buaya keluar ke tepi sungai menghampiri kancil yang terlihat senang, “Hei Kancil! Ada apa kamu ke sungai? Apa kamu mau menjadi santapan kami?” tanya buaya itu pada kancil.Kancil pun menjawab pertanyaan buaya dengan senang hati, “Aku mempunyai berita baik untuk kalian semua, aku membawa daging segar dari raja dan diperintahkan untuk menghitung jumlah buaya yang ada di sungai. Kalian cukup berjajar di sungai dan nanti akan aku hitung.” Merasa senang mendengar kabar kancil membawa daging segar untuk ia dan teman-teman buaya lainnya, buaya tadi kemudian menyanggupi permintaan kancil dan memanggil seluruh buaya yang berada di sungai untuk berjejer hingga membentuk jembatan. “Sudah siap!” kata semua buaya bersemangat. Kancil pun dengan girang melompati buaya dan pura-pura menghitung buaya-buaya yang sudah berjejer membentuk jembatan itu.Setelah sampai ujung, kancil pun melompat ke tepi sungai. Lalu ia berkata, “Terima kasih para buaya, berkat kalian, aku jadi bisa menyebrangi sungai ini.” Setelah berkata seperti itu pada buaya, kancil langsung berlari kencang meninggalkan buaya yang marah karena perbuatannya. Kancil pun dengan bebas memakan buah-buahan yang ada di sebrang sungai untuk menghilangkan rasa laparnya.</Text>,no:5},        
  
                ],
        };
    }
    render(){
        return(
        <View style={styles.wrapper}>
                <View>
                    <FlatList
                    data = {this.state.nurul}
                    renderItem = {({item,index}) => (
                        <View style={{marginBottom:10}}>
                            {item.poto}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                    numColumns = {numColumns}
                    />
                </View>
        </View>

        )
    }
}



const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
       
    },
    ukur : {
        height : WIDTH/(numColumns*1),
        justifyContent:'center',
        padding:5,
        borderRadius:4,
    },
    poto : {
        
    }
    
});
export default dongeng1;