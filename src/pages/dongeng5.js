import React, { Component } from 'react';
import {Image, StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList, Dimensions} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class dongeng1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nurul : [
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>DAHULU kala, di kaki sebuah gunung di daerah Bengkulu hiduplah seorang wanita tua dengan tiga orang anaknya. Mereka sangat miskin dan hidup hanya dari penjualan hasil kebunnya yang sangat sempit. Pada suatu hari, perempuan tua itu sakit keras. Orang pintar di desanya itu meramalkan bahwa wanita itu akan tetap sakit apabila tidak diberikan obat khusus. Obatnya adalah daun-daunan hutan yang dimasak dengan bara gaib dari puncak gunung.Alangkah sedihnya keluarga tersebut demi mengetahui kenyataan itu. Persoalannya adalah bara dari puncak gunung itu konon dijaga oleh seekor ular gaib. Menurut cerita penduduk desa, ular tersebut akan memangsa siapa saja yang mencoba mendekati puncak gunung itu.</Text>,no:1},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Di antara ketiga anak perempuan ibu tua itu, hanya si Bungsu yang menyanggupi persyaratan tersebut. Dengan perasaan takut ia mendaki gunung kediaman si Ular n’Daung. Benar seperti cerita orang, tempat kediaman ular ini sangatlah menyeramkan. Pohon-pohon di sekitar gua itu besar dan berlumut. Daun-daunnya menutupi sinar matahari sehingga tempat tersebut menjadi temaram.</Text>,no:2},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Belum habis rasa khawatir si Bungsu, tiba-tiba ia mendengar suara gemuruh dan raungan yang keras. Tanah bergetar. Inilah pertanda si Ular n’Daung mendekati gua kediamannya. Mata ular itu menyorot tajam dan lidahnya menjulur-julur. Dengan sangat ketakutan si Bungsu mendekatinya dan berkata, “Ular yang keramat, berilah saya sebutir bara gaib guna memasak obat untuk ibuku yang sakit. Tanpa diduga, ular itu menjawab dengan ramahnya, “Bara itu akan kuberikan kalau engkau bersedia menjadi istriku!”</Text>,no:3},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Si Bungsu menduga bahwa perkataan ular ini hanyalah untuk mengujinya. Maka ia pun menyanggupinya. Keesokan harinya setelah ia membawa bara api pulang, ia pun menepati janji pada ular n’Daung. Ia kembali ke gua puncak gunung untuk diperistri si ular. Alangkah terkejutnya si Bungsu menyaksikan kejadian ajaib. Yaitu, pada malam harinya, ternyata ular itu berubah menjadi seorang ksatria tampan bernama Pangeran Abdul Rahman Alamsjah.Pagi harinya, ia akan kembali menjadi ular. Hal itu disebabkan karena ia disihir oleh pamannya menjadi ular. Pamannya tersebut menghendaki kedudukannya sebagai calon Raja.</Text>,no:4},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Setelah kepergian si Bungsu, ibunya menjadi sehat dan hidup dengan kedua kakaknya yang pendengki. Mereka ingin mengetahui apa yang terjadi dengan si Bungsu. Maka mereka pun berangkat ke puncak gunung. Mereka tiba di sana pada malam hari.Alangkah terkejutnya mereka saat menjumpai seorang pria tampan di sana, bukannya seekor ular ganas. Timbul perasaan iri dalam diri mereka bahwa pria itu telah memperistri adiknya. Mereka pun menyusun sebuah rencana jahat untuk menyingkirkan adiknya.</Text>,no:5},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Mereka mengendap ke dalam gua dan mencuri kulit ular itu, kemudian mereka membakar kulit ular tersebut. Mereka mengira dengan demikian ksatria tersebut akan marah dan mengusir adiknya. Tetapi yang terjadi justru kebalikannya. Dengan dibakarnya kulit ular tersebut, secara tidak sengaja mereka membebaskan pangeran itu dari kutukan. Ketika menemukan kulit ular itu terbakar. Pangeran menjadi sangat gembira. Ia berlari dan memeluk si Bungsu. Diceritakannya bahwa sihir pamannya itu akan sirna jika ada orang yang secara sukarela membakar kulit ular itu.</Text>,no:6},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Kemudian, si Ular n’Daung yang sudah selamanya menjadi Pangeran Alamsjah memboyong si Bungsu ke istananya. Pamannya yang jahat diusir dari istana. Si Bungsu pun kemudian mengajak keluarganya tinggal di istana, tetapi kedua kakaknya yang sirik menolak karena merasa malu akan perbuatannya. [</Text>,no:7},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}></Text>,no:8},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}></Text>,no:9},   
              
            ],
        };
    }
    render(){
        return(
        <View style={styles.wrapper}>
                <View>
                    <FlatList
                    data = {this.state.nurul}
                    renderItem = {({item,index}) => (
                        <View style={{marginBottom:10}}>
                            {item.poto}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                    numColumns = {numColumns}
                    />
                </View>
        </View>

        )
    }
}



const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
       
    },
    ukur : {
        height : WIDTH/(numColumns*1),
        justifyContent:'center',
        padding:5,
        borderRadius:4,
    },
    poto : {
        
    }
    
});
export default dongeng1;