import React, { Component } from 'react';
import {Image, StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList, Dimensions} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class dongeng1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nurul : [
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Pada suatu ketika, hiduplah dua ekor kancil bersaudara. Mereka berdua menghuni hutan belantara yang sangat subur. Kakaknya bernama Manggut sedangkan adiknya bernama Kanca. Meskipun mereka bersaudara, mereka berdua ternyata memiliki sifat yang sangat berbeda. Kanca rajin dan baik hati, sedangkan Manggut pemalas, jahil dan suka berbohong. Suatu hari Manggut sedang kelaparan, ia pun berusaha mencari dan menemukan rerumputan hijau di seberang sungai kecil yang dangkal. Namun sayang sekali. ia enggan menyeberang karena Manggut sangatlah malas. Akhirnya Manggut pun mencuri makanan milik adiknya. Sewaktu Kanca ingin makan, dia terkejut karena makanan jatahnya sudah habis. Kanca lalu bertanya kepada Manggut di mana makanannya.Manggut bohong. "Makananmu habis dicuri Tikus," katanya.“Ah, mana mungkin dimakan tikus,” balas Kanca.</Text>,no:1},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>“Iya! Masa kamu tidak percaya dengan kakak sendiri,” jawab Manggut meyakinkan Mula-mulanya Kanca tidak percaya dengan ucapan Manggut. Tetapi setelah Manggut mengatakannya berkali-kali, akhirnya dia percaya juga. Namun ternyata, tanpa sepengetahuan Manggut, Kanca mencoba memanggil Tikus ke rumahnya, meminta pertanggungjawaban. Esok paginya, Tikus datang memenuhi panggilan Kanca. Melihat Tikus datang, raut muka Manggut terlihat agak memucat. “Tikus, apakah kamu yang kemarin mencuri makananku?” tanya Kanca pada Tikus.  “Hah? Mencuri makananmu? Berpikir saja aku belum pernah!” jawab Tikus. Tiba-tiba saja Manggut menyela pembicaraan mereka dan berusaha memojokkan Tikus, “Ah, Tikus! Kamu ini membela diri saja! Sudah, Kanca! Dia pasti berbohong. Berbeda dengan sang kakak, Kanca terlihat mempercayai ucapan Tikus. </Text>,no:2},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>“Ya, sudahlah kalau memang kamu tidak mengambilnya! Mumpung kamu berada di sini, aku mau minta tolong ambilkan makanan di seberang sungai sana. Tadi aku juga mengambil makanan dari sana, kok!” kata Kancil pada Tikus. Tikus pun setuju memenuhi permintaan Kanca. Kemudian ia segera pergi ke tepi sungai, membuat sampan untuk menuju seberang sungai. Sebenarnya Tikus tahu kalau Manggutlah yang telah mencuri makanan itu. Sementara itu, tidak ingin kalah dari Tikus, Manggut cepat-cepat menyeberangi sungai. Ia hendak memasang perangkap tikus agar Tikus terjebak dan tidak bisa kembali.</Text>,no:3}, 
                ],
        };
    }
    render(){
        return(
        <View style={styles.wrapper}>
                <View>
                    <FlatList
                    data = {this.state.nurul}
                    renderItem = {({item,index}) => (
                        <View style={{marginBottom:10}}>
                            {item.poto}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                    numColumns = {numColumns}
                    />
                </View>
        </View>

        )
    }
}



const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
       
    },
    ukur : {
        height : WIDTH/(numColumns*1),
        justifyContent:'center',
        padding:5,
        borderRadius:4,
    },
    poto : {
        
    }
    
});
export default dongeng1;