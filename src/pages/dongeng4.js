import React, { Component } from 'react';
import {Image, StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList, Dimensions} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class dongeng1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nurul : [
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Diceritakan pada zaman dahulu, hiduplah seorang Mama bernama Dayang Sumbi yang tinggal bersama anaknya bernama Sangkuriang. Keduanya tinggal di sebuah desa bersama dengan seekor anjing kesayangan mereka yaitu Tumang. Sebelum hidup berdua bersama anaknya, Dayang Sumbi menikah dengan titisan dewa yang telah dikutuk menjadi hewan dan dibuang ke bumi.Tanpa mereka sadari, sebenarnya mereka hidup bertiga bersama suami Dayang Sumbi dan papa dari Sangkuriang yang berubah menjadi anjing kutukan tadi.Setelah melewati masa bersama anaknya, Sangkuriang pun tumbuh menjadi pemuda dengan paras memesona serta tubuh yang gagah dan kuat.Sangkuriang tumbuh menjadi anak pemberani yang senang berburu, ia pun selalu ditemani si Tumang yang merupakan titisan anjing dari papa kandungnya sendiri.Pada suatu hari, Dayang Sumbi meminta Sangkuriang untuk mencarikannya kijang karena sang Mama menghendaki memakan hati kijang saat itu. Sangkuriang dengan ditemani si Tumang berburu ke hutan untuk mendapatkan kijang sesuai keinginan Dayang Sumbi.</Text>,no:1},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Saat di hutan, Sangkuriang melihat seekor kijang tengah merumput dibalik semak belukar. Sangkuriang pun memerintahkan Tumang untuk mengejar kijang tersebut. Namun ada hal aneh yang terjadi pada anjing piarannya itu, si Tumang yang biasanya penurut kini menolak perintah Sangkuriang untuk mengejar kijang tadi.Sangkuriang pun marah dan mengatakan, "Jika engkau tetap tidak menuruti perintahku, niscaya aku akan mebunuhmu.”Ancaman tersebut tidak dipedulikan si Tumang yang membuat Sangkuriang semakin kesal dan marah.Sangkuriang pun akhirnya membunuh Tumang dan mengambil hati anjing itu untuk diberikan kepada Dayang Sumbi sebagai pengganti anjing kijang yang tak berhasil ia dapatkan.Tanpa disadari Dayang Sumbi, ternyata hati yang diberikan anaknya adalah hati suaminya yang telah dibunuh oleh anak mereka sendiri. Dayang Sumbi baru mengetahui setelah memasak dan memakan hati itu. Betapa murkanya Dayang Sumbi ketika mengatahui bahwa hati si Tumang lah yang diberikan Sangkuriang padanya.</Text>,no:2},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Dayang Sumbi kemudian meraih gayung yang terbuat dari tempurung kelapa dan memukul kepala Sangkuriang sambil mengatakan yang seusungguhnya, "Tumang itu papamu, Sangkuriang!"Mendapat perlakuan dari Dayang Sumbi seperti itu, Sangkuriang pun marah dan sakit hati. Ia tak rela mamanya begitu padanya. Sangkuriang berpikir bahwa Dayang Sumbi lebih menyayangi si Tumang dibandingkan dirinya. Maka tanpa berpamitan, Sangkuriang pun pergi mengembara ke arah timur.Setelah kepergian Sangkuriang, Dayang Sumbi mengaku menyesal atas perbuatannya kepada anaknya sendiri. Ia pun memohon ampun kepada para dewa atas kesalahannya tersebut. Mendengar permohonan Dayang Sumbi, mereka menerima permintaan maaf itu dan mengaruniakan kecantikan abadi kepada Dayang Sumbi.Dilain sisi, Sangkuriang yang terus mengembara tanpa tujuan pasti, kini tumbuh menjadi lelaki dewasa yang memiliki paras dan tubuh yang dapat memikat banyak perempuan. Tanpa sadar, setelah bertahun lamanya mengembara ia kembali ke tempat dimana dulu dilahirkan.Sangkuriang berhenti ke salah satu pondok guna meminta air minum kepada pemilik pondok tersebut. Tanpa disadari, ia terpesona dengan kecantikan Dayang Sumbi yang abadi.</Text>,no:3},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Ia tak mengetahui bahwa perempuan berparas menawan yang ia temui itu adalah mama kandungnya sendiri.Begitu pun yang terjadi pada Dayang Sumbi, melihat sosok lelaki gagah nan sakti dihadapannya, ia tak menyadari bahwa lelaki tersebut adalah Sangkuriang anaknya sendiri.Dari situlah tumbuh rasa simpat dan cinta, sampai akhirnya mereka merencanakan pernikahan.Sebelum melangsungkan pernikahan, Sangkuriang yang mengganti namanya dengan Jaka ini berniat untuk berburu ke hutan. Dayang Sumbi pun membantu Jaka calon suaminya itu untuk mengenakan penutup kepala.Betapa terkejutnya Dayang Sumbi saat melihat luka di kepala calon suaminya itu.Luka tersebut mengingaatkannya pada anak laki-lakinya yang telah meninggalkannya dulu.Ia sangat yakin bahwa lelaki gagah yang akan menikahinya ini adalah Sangkuriang anaknya.Melihat bekas luka tadi, Dayang Sumbi kemudian menceritakan pada lelaki tersebut bahwa dirinya adalah Dayang Sumbi, orangtua kandung dari Sangkuriang yang telah bertahun-tahun lamanya menghilang.Namun, Sangkuriang yang telah dibutakan oleh hawa nafsu tidak memperdulikan penjelasan Dayang Sumbi dan tetap bersikukuh menikahinya Apa itu syarat? Syarat adalah janji. Menurut KBBI Syarat diajukan sebagai tuntutan atau permintaan yang harus dipenuhi. Begitu pula syarat yang diberikan kepada Sangkuriang, harus ditepati terlebih dulu baru kemudia Dayang Sumbi bersedia mengabulkan keinginannya.</Text>,no:4},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Sangkuriang yang bertekad tetap ingin menikahi Dayang Sumbi, kemudian melamar perempuan itu untuk dinikahinya.Untuk menghentikan pernikahan itu, Dayang Sumbi pun memberikan sebuah permintaan sebagai syarat untuk menerima lamaran dari Sangkuriang.Dayang Sumbi mengajukan permintaan yang sangat berat yaitu meminta Sangkuriang membuat bendungan pada sungai Citarum dan di dalam danau tersebut terdapat perahu besar.Namun, yang membuat permintaan itu berat adalah karena perkataan Dayang Sumbi, "Sebelum fajar terbit, kedua permintaanku itu harus telah selesai engkau kerjakan.”Tanpa ragu, Sangkuriang menyanggupi permintaan dari Dayang Sumbi, "Baiklah, aku akan memenuhi permintaanmu.”Sangkuriang pun memulai aksinya untuk membuat perahu dengan menebang pohon besar. Sementara cabang dan ranting pohon yang dibutuhkan ditumpuk sampai suatu hari menjemla menjadi gunung Burangrang. Perahu besar pun berhasil dibuat Sangkuriang. Setelahnya, lelaki gagah nan sakti itu memanggil para makhluk halus untuk membantunya membendung alirang sungai Citarum yang deras untuk dibuat sebuah danau sesuai permintaan Dayang Sumbi.Semua yang dilakukan Sangkuriang kemudian membuat Dayang Sumbi merasa cemas karena pekerjaannya sebentar lagi selesai sebelum berganti hari.</Text>,no:5},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Dayang Sumbi pun mencari cara untuk menggagalkan rencana pernikahan dengan anak kandungnya sendiri dengan memohon pertolongan para dewa.Setelah berdoa, Dayang Sumbi mendapat petunjuk untuk menebarkan boeh rarang (kain putih hasil tenunan).Setelah itu Dayang Sumbi berkeliling dan memaksa ayam jantan berkokok disaat waktu masih malam. Mendengar suara ayam sudah bersuara, para jin yang membantu pekerjaan Sangkuriang pun sangat ketakutan ketika mengetahui bahwa fajar telah tiba.Mereka kemudian menghilang kesegala penjuru dan meninggalkan Sangkuriang dengan pekerjaannya yang belum selesai.Tentu saja hal ini membuat Sangkuriang marah besar karena merasa dicurangi oleh calon istrinya sendiri.Sangkuriang meyakini bahwa fajar sesungguhnya belum tiba dan masih ada waku untuk ia menyelesaikan danau tersebut.</Text>,no:6},   
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Sangkuriang lantas murka dengan menjebol bendungan di Sanghyang Tikoro, kemudian aliran sungai Citarum dilemparkannya ke arah timur hingga menjelma menjadi gunung Manglayang. Air yang semula memenuhi danau tersebut pun surut.Sangkuriang kemudian dengan kekuatan saktinya menendang perahu yang tadi ia buat hingga jauh dan jatuh terlungkup sampai menjelma menjadi sebuah gunung yang kemudian disebut gunung Tangkuban Perahu.Masih dalam hawa amarah yang besar, Sangkuriang yang mengetahui ini semua adalah siasat Dayang Sumbi untuk menggagalkan pernikahan dengannya. Kemarahan yang terus meluap itu kemudian membuat Sangkuriang mengejar Dayang Sumbi yang merasa ketakutan hingga menghilang di sebuah bukit.Selain perahu yang menjelma menjadi gunung Tangkuban Perahu, bukit yang menjadi tempat menghilangnya Dayang Sumbi pun ikut menjelma menjadi gunung Putri.Sedangkan Sangkuriang yang tidak berhasil menemukan Dayang Sumbi akhirnya ikut menghilang ke alam gaib.</Text>,no:7},   
     
            ],
        };
    }
    render(){
        return(
        <View style={styles.wrapper}>
                <View>
                    <FlatList
                    data = {this.state.nurul}
                    renderItem = {({item,index}) => (
                        <View style={{marginBottom:10}}>
                            {item.poto}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                    numColumns = {numColumns}
                    />
                </View>
        </View>

        )
    }
}



const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
       
    },
    ukur : {
        height : WIDTH/(numColumns*1),
        justifyContent:'center',
        padding:5,
        borderRadius:4,
    },
    poto : {
        
    }
    
});
export default dongeng1;