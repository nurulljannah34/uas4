import React, { Component } from 'react';
import {Image, StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList, Dimensions} from 'react-native';

const numColumns = 2;
const WIDTH = Dimensions.get('window').width;

class dongeng1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nurul : [
                {poto : <Image source={require('../assets/a.jpg')} style={styles.poto}/>, judul : 'Si Kancil dan buaya ', tombol : <TouchableOpacity onPress={() => this.props.navigation.navigate('Si Kancil dan buaya')}><Text>Selengkapnyaa</Text></TouchableOpacity>,no:1},
                {poto : <Image source={require('../assets/b.jpg')} style={styles.poto}/>, judul : 'Si Kancil dan siput adu pintar ', tombol : <TouchableOpacity onPress={() => this.props.navigation.navigate('Si Kancil dan siput adu pintar')}><Text>Selengkapnyaa</Text></TouchableOpacity>,no:2},
                {poto : <Image source={require('../assets/c.jpg')} style={styles.poto}/>, judul : 'Si Kancil dan tikus', tombol : <TouchableOpacity onPress={() => this.props.navigation.navigate('Si Kancil dan tikus')}><Text>Selengkapnyaa</Text></TouchableOpacity>,no:3},
                {poto : <Image source={require('../assets/d.jpg')} style={styles.poto}/>, judul : 'Sangkuriang dan tangkuban perahu', tombol : <TouchableOpacity onPress={() => this.props.navigation.navigate('Sangkuriang dan tangkuban perahu')}><Text>Selengkapnyaa</Text></TouchableOpacity>,no:4},
                {poto : <Image source={require('../assets/e.jpg')} style={styles.poto}/>, judul : 'Dongen legenda ular ndaung ', tombol : <TouchableOpacity onPress={() => this.props.navigation.navigate('Dongen legenda ular ndaung')}><Text>Selengkapnyaa</Text></TouchableOpacity>,no:5},
                {poto : <Image source={require('../assets/f.jpg')} style={styles.poto}/>, judul : 'Legenda keong mas ', tombol : <TouchableOpacity onPress={() => this.props.navigation.navigate('Legenda keong mas')}><Text>Selengkapnyaa</Text></TouchableOpacity>,no:6},
                {poto : <Image source={require('../assets/g.jpg')} style={styles.poto}/>, judul : 'Lutung kasarung dan purba sari ', tombol : <TouchableOpacity onPress={() => this.props.navigation.navigate('Lutung kasarung dan purba sari')}><Text>Selengkapnyaa</Text></TouchableOpacity>,no:7},
                {poto : <Image source={require('../assets/h.jpg')} style={styles.poto}/>, judul : 'Ande-ande lumut', tombol : <TouchableOpacity onPress={() => this.props.navigation.navigate('Ande-ande lumut')}><Text>Selengkapnyaa</Text></TouchableOpacity>,no:8},
               
            ],
        };
    }
    render(){
        return(
        <View style={styles.wrapper}>
                <View style={{flex:1}}>
                    <Text style={{fontSize:70}}>DONGENGKUU</Text>
                </View>
                <View style={{flex:10}}>
                    <FlatList
                    data = {this.state.nurul}
                    renderItem = {({item,index}) => (
                        <View style={styles.ukur}>
                            <Text>{item.poto}</Text>
                            <Text style={{marginTop:10,textAlign:'center'}}>{item.judul}</Text>
                            <Text style={{marginTop:15,marginBottom:50,textAlign:'center'}}>{item.tombol}</Text>
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                    numColumns = {numColumns}
                    />
                </View>
        </View>

        )
    }
}



const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flex:5
    },
    ukur : {
        height : WIDTH/(numColumns*1),
        justifyContent:'center',
        padding:5,
        borderRadius:4,
    },
    poto : {
        width:170,
        height:170,
    }
    
});
export default dongeng1;