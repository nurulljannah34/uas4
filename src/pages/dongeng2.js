import React, { Component } from 'react';
import {Image, StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList, Dimensions} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class dongeng1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nurul : [
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Di suatu hari yang cerah, terdapat seekor binatang yang terkenal sangat licik di dalam hutan, yaitu adalah Si Kancil. Pada hari itu, Kancil sedang bersantai di bawah pohon besar dengan hembusan angin sepoi-sepoi yang membuat Kancil menjadi mengantuk. Untuk mengusir rasa kantuknya, ia akhirnya memutuskan untuk jalan-jalan menelusuri hutan. Sambil berjalan, Kancil membusungkan dadanya dan berkata, "Siapa di hutan ini yang tidak mengenalku. Si pintar, si cerdik yang banyak akal. Setiap masalah pasti dapat ku selesaikan dengan mudah,” kata Si Kancil.Setelah berjalan cukup jauh, akhirnya ia sampai di tepi sungai. Kancil pun segera minum untuk menghilangkan rasa hausnya sambil terus berkata-kata memuji dirinya sendiri.Tanpa Kancil sadari, ternyata ia sedang diperhatikan oleh seekor Siput yang sedang duduk di balik sebuah batu besar pinggir sungai. Karena mendengar ucapan Kancil Siput itu pun berkata,"Hei Kancil, asyik sekali kau ku lihat berbicara sendiri, ada apa? Apa kamu sedang bergembira ya?" tanya Siput pada Kancil.Kancil pun mendengar suara Siput dan mencari-cari asal suara tersebut, ia pun menjawab,</Text>,no:1},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>“Oh hai! ternyata kau Siput, sudah lama ya kau memperhatikanku ternyata? Sedang apa kau disana Siput? Meratapi dirimu yang kecil dan lelet ya? Hahahahaha," jawabnya. Siput pun terkejut dengan jawaban Kancil yang menghinanya dan membuatnya marah. Lalu Siput pun membalas ucapan Kancil. “"Hai Kancil! Aku tahu, kau memang terkenal binatang yang sangat cerdik dan cepat sedangkan diriku terkenal binatang yang sangat lambat berjalannya. Tapi kali ini aku sangat marah mendengar perkataanmu tadi dan aku menantangmu untuk lomba adu lari," ujar Siput.Mendengar tantangan dari Siput, Kancil pun menerima tantangannya karena ia tahu Siput tak akan mungkin mengalahkan dirinya. Hingga akhirnya mereka setuju lomba tersebut diadakan keesokan harinya.Di tempat lain, Siput sadar bahwa dirinya tak mampu mengalahkan Kancil. Siput akhirnya meminta tolong teman-temannya untuk membantunya dengan sebuah cara. Saat lomba dimulai, semua teman-teman Siput agar bersembunyi di jalur lomba yang akan mereka lalui.Teman-teman Siput harus muncul ketika mendengar suara Si Kancil dari kejauhan, sehingga Kancil akan mengira Siput akan selalu berada di depannya.Hari perlombaan pun tiba!Seluruh penghuni hutan menyaksikan perlobaan tersebut sampai suasananya pun sangat ramai. Mereka semua ingin mengetahui apakah Siput dapat mengalahkan Si Kancil yang terkenal cerdik dan sombong itu?</Text>,no:2},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Saat lomba dimulai, dengan angkuhnya Kancil langsung berlari dengan sangat cepat, ia pun tertawa sambil berkata:"Hahahahaha selamat tinggal Siput lelet, aku tunggu kau di garis finish nanti," ucapnya dengan sombong. Setelah ia berlari meninggalkan Siput cukup jauh di belakangnya, Kancil justru sangat terkejut karena ia melihat di depannya ada Siput yang sedang berjalan dengan santai.Yang sebenarnya itu adalah teman-teman Si Siput yang telah mendengar suara Kancil dari kejauhan dan mereka keluar dari persembunyiannya guna mengelabui Si Kancil.Karena merasa dikalahkan, Kancil pun dengan cepat melewati Siput tersebut, dan kejadian itu terus berulang hingga membuat Si Kancil menjadi kewalahan dan kelelahan karena Siput selalu berada beberapa Langkah di depan Kancil.Ketika Si Kancil hampir tiba di garis finish, ternyata Siput sudah mendekati garis finish terlebih dahulu dan membuat Si Kancil berpikir.Mengapa Siput yang seharusnya sudah ia tinggalkan jauh di belakang, tapi Siput itu malah terus berada di depannya dan sampai ke garis finish terlebih dahulu.Kancil terkejut dirinya kalah dengan Siput.Lalu sesampainya di garis finish Kancil pun berkata,"Tidak Mungkin! Bagaimana bisa kau lebih dulu sampai, padahal aku berlari sangat kencang meninggalkan kau jauh dibelakangku," ujar Si Kancil yang tak menerima kekalahannya.</Text>,no:3},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>.Kancil tidak mau kalah dengan seekor Siput yang kemarin ia ejek di sungai."Sudahlah Kancil akui saja kekalahan dirimu," jawab Siput dengan santai.Si Kancil menjadi heran dan masih belum dapat percaya kalau dirinya berhasil dikalahkan oleh hewan yang sering ia ejek "kecil dan lelet" tersebut.Si Kancil yang masih terheran-heran hingga tidak bisa mengucapkan apa pun, lalu tiba-tiba Siput pun berkata,"Sudahlah Kancil, tidak usah sedih. Aku tidak ingin hadiah apa-apa dari kamu. Aku hanya ingin kau tahu, janganlah menjadi sombong dengan kelebihan yang kau miliki. Semua makhluk hidup mempunyai kelebihan dan kekurangannya masing-masing, jadi jangan suka menghina dan menyepelekan makhluk hidup yang lainnya," ujar Siput.Setelah menyelesaikan ucapannya Siput pun pergi menyelam kedalam sungai dan tinggalah Si Kancil yang menyesal dan malu karena kalah dalam lomba lari dengan Siput.Sejak saat itu Kancil berjanji tidak akan menganggap remeh makhluk hidup lainnya.</Text>,no:4},  
                ],
        };
    }
    render(){
        return(
        <View style={styles.wrapper}>
                <View>
                    <FlatList
                    data = {this.state.nurul}
                    renderItem = {({item,index}) => (
                        <View style={{marginBottom:10}}>
                            {item.poto}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                    numColumns = {numColumns}
                    />
                </View>
        </View>

        )
    }
}



const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
       
    },
    ukur : {
        height : WIDTH/(numColumns*1),
        justifyContent:'center',
        padding:5,
        borderRadius:4,
    },
    poto : {
        
    }
    
});
export default dongeng1;