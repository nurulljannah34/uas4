import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';


const Splash = ({navigation}) => {
    useEffect(( ) => {
        setTimeout(( ) => {
            navigation.replace ('welcome');
        }, 5000);
    });
    return(
        <View style={styles.wrapper}>
         <Text style= {styles.welcomeText}>Yok Ngedongeng</Text>
         <Image source= {require('../assets/1.jpg')} style={{width:270,height:300}}></Image>
        </View> 
        );
    };

export default Splash;
const styles = StyleSheet.create({
   wrapper: {
       flex: 1,
       backgroundColor: 'white',
       alignItems: 'center',
       justifyContent: 'center',
   },
  
   welcomeText: {
       fontSize:24,
       fontWeight: 'bold',
       color: 'green',
       paddingBottom: 20,
   },
});
    