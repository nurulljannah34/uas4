import React, { Component } from 'react';
import {Image, StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList, Dimensions} from 'react-native';

const numColumns = 1;
const WIDTH = Dimensions.get('window').width;

class dongeng1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nurul : [
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Lutung Kasarung yang artinya Lutung yang Terserat, merupakan cerita pantun yang mengisahkan legenda masyarakat Sunda tentang perjalanan Sanghyang Guruminda dari Kahyangan, ia diturunkan ke Buana Panca Tengah atau Bumi, dalam wujud seekor lutung atau sejenis monyet.
                Dalam perjalanannya, Lutung bertemu dengan putri Purbasari Ayuwangi yang sedang diusir oleh Saudaranya, Purbararang. Cerita Lutung Kasarung ini sering muncul dalam bentuk buku cerita atau buku komik, serta dalam bentuk sinetron di televisi Indonesia.</Text>,no:1},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>menyiapkannya di bawah ini Pada zaman dahulu kala, hiduplah seorang putri yang bernama Purbasari. Ia merupakan anak bungsu dari enam orang kakak perempuan yaitu Purbararang, Purbadewata, Purbaendah, Purbakancana, Purbamanik, dan Purbaleuih. Mereka merupakan anak dari Prabu Tapa Agung yang merupakan raja dari kerajaan pasir batang. Si Bungsu, Purbasari memiliki sifat yang sangat baik, lembut, manis budi, dan suka menolong. Siapapun yang membutuhkan pertolongannya, Purbasari senang hati membantunya. Selain memiliki hati yang baik, Purbasari juga memiliki wajah yang cantik dan rupawan. Setiap orang yang melihatnya akan jatuh hati pada pandangan pertama. Namun sayangnya, sang Kakak Purbararang memiliki sifat yang sebaliknya. Walaupun berparas cantik, Purbararang dikenal memiliki sifat yang kasar, sombong, kejam, iri hati pada siapapun.com</Text>,no:2},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Setelah bertahta dalam waktu yang lama, Prabu Tapa Agung berniat turun tahta, dan berencana bahwa kerjaan dipimpin oleh Purbasari. Sang Prabu mengamati selama puluhan tahun bahwa Purbasari pantas mengantikannya. Bukan anak sulungnya alias Purbararang. Sang Prabu memikirkan bagaimana jika Purbararang menjadi pemimpin, maka ketentraman dan kedamaian rakyat bisa terganggu, atau bahkan rusak akibat kepemimpinan anak sulungnya yang punya sifat buruk. Dihadapan seluruh pembesar kerajaan dan tujuh putrinya, raja Prabu Tapa Agung menyerahkan takhtanya pada Purbasari. Sang Prabu kemudian meninggalkan istana kerajaannya untuk memulai hidup baru sebagai pertama. Purbararang pun marah karena tidak setuju takhta Kerajaan Pasir Batang diberikan kepada adiknya, bukan untuk dirinya. Selang satu hari sejak penobatan, Purbararang berencana mencelakai Purbasari. Ia menghubungi tunangannya, Indrajaya untuk meminta bantuan nenek sihir Nenek sihir yang jahat kemudian memberikan boreh, atau zat berwarna hitam yang dibuat dari tumbuhan kepada Purbararang.</Text>,no:3},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>“Semburkan boreh ini kewajah dan seluruh tubuh Purbasari” ujar nenek sihir pada Purbararang.Purbararang langsung melaksanakan pesan dari si nenek sihir. Boreh tersebut disemburkan ke wajah dan seluruh tubuh Purbasari. Akibatnya diseluruh tubuh Purbasari muncul bercak hitam yang mengerikan. Dengan kondisi tersebut Purbararang memiliki alasan untuk mengusir Purbasari dari istana dan menghentikannya menjadi ratu. “Orang yang dikutuk hingga memiliki penyakit mengerikan ini tidak pantas menjadi Ratu kerajaan Pasir Batang. Sudah seharusnya dia diasingkan ke hutan agar penyakitnya tidak menular,” perintah Purbararang. Kemudian Purbararang mengambil tahta Kerajaan Pasir Batang dan memerintahkan Uwak Batara yang merupakan penasihat istana untuk mengasingkan Purbasari ke hutan. Uwak Batara kemudian membawa Lutung Kasarung ke hutan dimana Purbasari diasingkan. Namun ia yakin bahwa Lutung Kasarung bukanlah hewan biasa, oleh karena itu Uwak Batara memberikan pesan kepada Lutung Kasarung saat bertemu Purbasari. “Lutung, puteri yang kamu temui adalah putri dari Prabu Tapa Agung. Ia adalah Putri yang baik hati dan seharusnya menjadi Ratu Kerajaan Pasir Batang. Hanya karena kekuatan jahatlah dia diasingkan dan tersingkir ke hutan ini. Oleh karena itu hendaklah engkau menjaga junjungan kami ini.” ujar Uwak Batara. Lutung Kasarung menganggukan kepala tanda mengerti. Maka saat itu, Lutung Kasarung menjadi penjaga yang juga sekaligus menjadi sahabat dekat Purbasari. Kehadiran Lutung Kasarung membuat kesedihan Purbasari perlahan sirna. Ia mendapatkan sahabat yang menghibur dan melindunginya. Untuk memenuhi kebutuhan sehari-hari, Lutung Kasarung memerintahkan para kera untuk membawa makanan dan buah-buahan untuk Purbasari</Text>,no:4},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Kelembutan hati, kebaikan dan sifat baik Purbasari membuat Lutung Kasarung mulai menyayangi Purbasari. Sedangkan sikap tanggung jawab, kepemimpinan dan kecerdasan dari Lutung Kasarung membuat Purbasari juga menyayanginya. Semakin lama keduanya tidak mau dipisahkan lagi. Tanpa diketahui Purbasari, Lutung Kasarung memohon kepada sang Mama, Sunan Ambu untuk dibuatkan taman yang indah dengan tempat pemandian untuk Purbasari. Sunan Ambu lantas meminta para dewa dan para bidadari turun ke bumi untuk mewujudkan keinginan dari putranya. Para Dewa dan Bidadari membuatkan taman dan tempat mandi yang sangat indah untuk Purbasari. Pancurannya terbuat dari emas murni. Dinding dan lantainya terbuat dari batu pualam. Air telaga yang mengalir berasal dari telaga kecil yang murni bersih dan dengan doa-doa dari para dewa. Para Dewa dan Bidadari menyebut taman yang indah itu Jamban Salaka Selain dibuatkan telaga dan taman yang indah, para bidadari juga menyiapkan beberapa pakaian indah untuk Purbasari. Pakaian itu sangat indah dan lembut. Terbuat dari awan yang lembut dengan hiasan batu-batu permata dari dalam lautan. Tak ada pakaian di dunia ini yang mampu menandingi keindahan pakaian Purbasari. Pada saat Purbasari melihat telaga dengan pancuran yang indah, ia segera mandi untuk membersihkan diri. Pada saat itulah boreh kutukan yang menempel di wajah dan tubuhnya perlahan sirna. Kecantikannya yang duluh telah kembali, Lutung Kasarung yang melihatnya menjadi terperangah tidak menyangka orang yang selama ini didekatnya adalah perempuan yang sangat cantik hingga dapat mengalahkan kecantikan dari Sunan Ambu.</Text>,no:5},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Lutung Kasarung dan Purbasari sangat bahagia dengan keadaannya. Walaupun Purbasari telah kembali kewujudnya yang cantik rupawan, kasih sayang Purbasari terhadap Lutung Kasarung tidak berkurang, namun semakin bertambah Kabar mengenai kembalinya kecantikan Purbasari didengar oleh Purbararang. Purbararang tidak percaya dengan berita ini, dia masih percaya diri karena tahu bahwa boreh yang disemburkan kepada Purbasari mengandung kutukan yang sangat jahat dan kuat. Purbararang lantas mengajak Indrajaya untuk melihat kebenaran berita tersebut. Dan betapa terkejutnya ia melihat Purbasari telah kembali kesosoknya yang cantik rupawan. Purbasari terlihat semakin mempesona dengan balutan pakaian dari para bidadari. Purbararang pun khawatir karena telah kembalinya Purbasari akan mengancam takhta yang saat ini dikuasainya. Sehingga ia mencari cara untuk kembali menyingkirkan adiknya tersebut, bahkan kali ini ia berniat menyingkirkan Purbasari untuk selama-lamanya.Purbararang lantas menantang Purbasari untuk beradu panjang rambut. “Jika rambutku lebih panjang dibandingkan rambut Purbasari, maka leher Purbasari harus dipenggal oleh algojo kerajaan,” tantang Purbararang Sayangnya, Purbararang kembali menelan kekecewaan yang besar setelah terbukti rambutnya yang sebetis kalah panjang dengan rambut Purbasari yang sepanjang tumit. Purbararang sangat malu mendapati kekalahannya.</Text>,no:6},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Untuk menutupi kekalahannya, Purbararang memberikan tantangan baru untuk Purbasari. Tak tanggung-tanggung, kini tantangannya diucapkan didepan seluruh masyarakat Kerajaan Pasir Batang. Dengan suara lantang agar didengar warga masyarakat. “Jika wajah tunanganmu lebih tampan dibandingkan wajah tunanganku, takhta Pasir Batang akan kuserahkan kepadamu. Namun jika sebaliknya, maka engkau hendaklah merelakan lehermu dipenggal algojo kerajaan,” sebut Purbararang. Purbasari paham ia tak akan mampu menang pada tantangannya kali ini. Namun rasa sayangnya pada Lutung Kasarung membuatnya tegar. Kemudian Purbasari menggenggam tangan Lutung Kasarung.“ Aku mencintaimu dan ingin engkau menjadi suamiku.” ucapnya kepada Lutung Kasarung.</Text>,no:7},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Air mata kemudian berlinang mengalir dikedua pipinya. Lutung Kasrung kemudian balas menggenggam tangan Purbasari kemudian mengusap air mata Purbasari. Purbasari yang melihat terbawa terbahak-bahak dan merendahkan Lutung Kasarung.</Text>,no:8},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>“Monyet hitam itu tunanganmu?” Namun sebelum Purbararang memerintahkan algojo untuk memenggal Purbasari. Lutung Kasarung kemudian duduk bersila dengan mata terpejam. Mulutnya terlihat menyebutkan mantra-mantra ajaib. Tiba-tiba asap tebal menyelimuti tubuh Lutung Kasarung. sosok Pangeran Guruminda yang tampan dan gagaTerkejutlah semua warga yang hadir ditempat itu mendapati keajaiban yang luar biasa tersebut. Betapa tampannya Pangeran Guruminda, bahkan sangat jauh melebihi ketampanan Indrajaya tunangan dari Purbararang. Pangeran Guruminda lantas mengumumkan bahwa ratu kerajaan Pasir Batang yang sebenarnya adalah Purbasari. Purbararang telah mengalami kekalahan dari tantangan yang dibuatnya sendiri. Dalam kondisi seperti itu, Purbararang tidak dapat menyangkal dan mau tidak mau mengakui kekalahannya. Namun, tidak ada lagi yang dapat ia lakukan selain menyerakan takhta kerajaan pasir batang kepada adiknya Purbasari.Karena merasa kalah, Purbararang pun memohon ampun atas kejahatan yang telah dilakukannya bersama Indrajaya. Karena memiliki hati yang baik, Purbasari memaafkan kesalahan kakak sulungnya tersebut. Sejak saat itu Purbasari kembali bertakhta sebagai Ratu.</Text>,no:9},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Semua rakyat sangat bergembira menyambut ratu yang baru, dan sekaligus terlepas dari belenggu pemerintahan Purbararang yang jahat. Mereka kemudian semakin berbahagia mengetahui bahwa Ratu Mereka Purbasari menikah dengan Pangeran Guruminda. Purbasari dan Pangeran Guruminda pun hidup bahagia selamanya.</Text>,no:10},
                {poto : <Text style={{textAlign:'justify',paddingTop:5}}>Nah itulah cerita tentang dongeng Lutung Kasarung dan Purbasari yang berasal dari cerita masyarakat Sunda. Pesan yang dapat Mama ajarkan pada anak adalah kebenaran dan kebaikan dapat mengalahkan kejahatan. Orang yang melakukan kebenaran, pada akhirnya akan keluar sebagai pemenang</Text>,no:11},
                
  
                ],
        };
    }
    render(){
        return(
        <View style={styles.wrapper}>
                <View>
                    <FlatList
                    data = {this.state.nurul}
                    renderItem = {({item,index}) => (
                        <View style={{marginBottom:10}}>
                            {item.poto}
                        </View>
                    )}
                    keyExtractor = {(item) => item.no}
                    numColumns = {numColumns}
                    />
                </View>
        </View>

        )
    }
}



const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
       
    },
    ukur : {
        height : WIDTH/(numColumns*1),
        justifyContent:'center',
        padding:5,
        borderRadius:4,
    },
    poto : {
        
    }
    
});
export default dongeng1;