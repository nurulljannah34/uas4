import React from 'react';
import{createStackNavigator} from '@react-navigation/stack';

import Splash from'../pages/splash';
import welcomeAuth from '../pages/welcome';
import Dongeng1 from '../pages/dongeng1';
import Dongeng2 from '../pages/dongeng2';
import Dongeng3 from '../pages/dongeng3';
import Dongeng4 from '../pages/dongeng4';
import Dongeng5 from '../pages/dongeng5';
import Dongeng6 from '../pages/dongeng6';
import Dongeng7 from '../pages/dongeng7';
import Dongeng8 from '../pages/dongeng8';


const Stack = createStackNavigator();

const Route = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen
            name="splash"
            component={Splash}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="welcome"
            component={welcomeAuth}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Si Kancil dan buaya"
            component={Dongeng1}
            />
            <Stack.Screen
            name="Si Kancil dan siput adu pintar"
            component={Dongeng2}
            />
            <Stack.Screen
            name="Si Kancil dan tikus"
            component={Dongeng3}
            />
            <Stack.Screen
            name="Sangkuriang dan tangkuban perahu"
            component={Dongeng4}
            />
            <Stack.Screen
            name="Dongen legenda ular ndaung"
            component={Dongeng5}
            />
            <Stack.Screen
            name="Legenda keong mas"
            component={Dongeng6}
            />
            <Stack.Screen
            name="Lutung kasarung dan purba sari"
            component={Dongeng7}
            />
            <Stack.Screen
            name="Ande-ande lumut"
            component={Dongeng8}
            />
            
        
            </Stack.Navigator>
            
    );
};

export default Route;